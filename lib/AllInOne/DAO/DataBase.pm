package AllInOne::DAO::DataBase;

use strict;
use warnings;
use DBI;
use Params::Validate qw(:all);
use AllInOne::Util::ClassUtils qw(GetClass);

sub new {
    my $class  = AllInOne::Util::ClassUtils::GetClass(shift);
    my %params = validate_with(
        params => \@_,
        spec   => {
            host     => { type => SCALAR, },
            user     => { type => SCALAR },
            port     => { type => SCALAR },
            password => { type => SCALAR },
            dbname   => { type => SCALAR },
            driver   => {
                type    => SCALAR,
                default => 'Pg'
            },
        }
    );
    my $self = bless {%params}, $class;
    my ( $host, $user, $port, $password, $dbname, $driver ) =
      @$self{qw(host user port password dbname driver)};
    my $dsn = "dbi:$driver:dbname=$dbname;host=$host;port=$port;";
    $self->{dsn} = $dsn;
    $self->{dh}  = DBI->connect( $dsn, $user, $password,
        { AutoCommit => 0, RaiseError => 1, PrintError => 0 } );
    return $self;
}

sub AUTOLOAD {
    our $AUTOLOAD;
    my $self = shift;
    my $name = $AUTOLOAD =~ s/.*:://r;
    if (wantarray) {
        my @retorno = $self->{dh}->$name(@_);
        return @retorno;
    } else {
        my $retorno = $self->{dh}->$name(@_);
        return $retorno;
    }
}
1;
