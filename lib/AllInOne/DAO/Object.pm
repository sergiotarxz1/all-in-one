package AllInOne::DAO::Object;

use Moo::Role;
use Params::Validate qw(:all);
use AllInOne::Util::Log qw(IS_LOGGER);

requires 'Get';
requires 'GetAll';
requires 'Delete';
requires 'Update';

has data_base => (
    is     => 'ro',
    isa    => sub { UNIVERSAL::can( 'connect', $_[0] ) },
    reader => '_DataBase',
);

has logger => (
    is     => 'ro',
    isa    => \&IS_LOGGER,
    reader => '_Logger',
);

sub _GetBy {
    my $self   = shift;
    my %params = validate_with(
        params => \@_,
        spec   => {
            params => { type => HASHREF, },
            get_by => { type => ARRAYREF, },
        },
    );
    my @get_by     = @{ $params{get_by} };
    my %params_get = %{ $params{params} };
    for (@get_by) {
        return $_ if exists $params_get{get_by};
    }
}
1;
