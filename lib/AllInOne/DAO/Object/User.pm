package AllInOne::DAO::Object::User;

use Moo;
use Params::Validate qw(:all);
use Const::Fast;

with 'AllInOne::DAO::Object';
const my $TABLE_NAME => 'User';

const my @GET_BY_COLUMNS => qw(id username);

sub Get {
    my $self   = shift;
    my %params = validate_with(
        params => \@_,
        spec =>
          { map { $_ => { type => SCALAR, required => 0 } } @GET_BY_COLUMNS, }
    );
    my $data_base = $self->_DataBase;
    my $get_by    = $self->_GetBy(
        params => \%params,
        get_by => [@GET_BY_COLUMNS]
    );
    unless ( defined $get_by ) {
        $self->_Logger->ERROR(
            'We are not getting the User because there is no query field');
        return;
    }
    my $user = $data_base->selectrow_hashref(
        "SELECT * FROM $TABLE_NAME  WHERE $get_by = ?",
        $params{$get_by}, );

}
sub GetAll { }
sub Delete { }
sub Update { }
1;
