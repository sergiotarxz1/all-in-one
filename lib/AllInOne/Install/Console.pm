#!/usr/bin/env perl
package AllInOne::Install::Console;
use strict;
use warnings;
use AllInOne::DAO::DataBase;
use Getopt::Long qw(:config gnu_getopt);
use Data::Dumper;
use Const::Fast;
use feature 'say';

const my @COMMANDS => ( 'install', 'remove', 'reset' );

sub get_command {
    my $pghost     = 'localhost';
    my $pgport     = 5432;
    my $pgdatabase = 'AllInOne';
    my $pguser;
    my $pgpassword;
    my $help;
    GetOptions(
        'H|pghost=s'     => \$pghost,
        'p|pgport=i'     => \$pgport,
        'd|pgdatabase=s' => \$pgdatabase,
        'u|pguser=s'     => \$pguser,
        'P|pgpassword=s' => \$pgpassword,
        'h|help'         => \$help,
    );
    if ($help) {
        my $help_text = <<EOF
This is the cli tool to install, remove or reset the AllInOne web application.

    -h | --help         It shows this message.
    -H | --pghost       Postgresql host.                            Default: "localhost"
    -p | --pgport       Postgresql port.                            Default: 5432.
    -d | --pgdatabase   Postgresql database.                        Default: "AllInOne"
    -u | --pguser       Postgresql user.
    -P | --pgpassword   Postgresql password. 
    -D | --datadir.     Where to store AllInOne data.               Default: "/var/lib/AllInOne/data"
    -I | --installdir   Where to install AllInOne.                  Default: "/var/lib/AllInOne/install"
    -f | --force        Overwrite existing AllInOne installation    Default: false

EOF
          ;
        say $help_text;
        return 0;
    }
    my $command = shift @ARGV;
}
get_command;
