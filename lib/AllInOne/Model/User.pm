package AllInOne::Model::User;

use Moo::Role;
use MooX::Types::MooseLike::Base qw(:all);

const my $valid_character_user_name => qr/\w|_/;

requires 'UserType';

has user_name => (
    is     => 'ro',
    reader => 'UserName',
    isa    => sub {
        $_ = shift;
        Str->($_) && /$valid_character_user_name{3,20}/;
    },
);
