package AllInOne::Util::ClassUtils;

require Exporter;

BEGIN {
    our @ISA = qw(Exporter);
    our @EXPORT_OK = qw(GetClass);
}

sub GetClass {
    my $self = shift;
    return $self unless ref $self;
    return ref $self;
}
