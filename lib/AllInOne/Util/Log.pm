package AllInOne::Util::Log;

use strict;
use warnings;

use Moo::Role;
use List::AllUtils;
use Exporter;

BEGIN {
    our @ISA = qw(Exporter);
    our @EXPORT_OK = qw(IS_LOGGER);
}

requires 'ERROR';
requires 'CRITICAL';
requires 'WARNING';
requires 'DEBUG';

sub IS_LOGGER {
    my $logger = shift;
    return List::AllUtils::all { UNIVERSAL::can $logger, $_ }
    qw(ERROR CRITICAL WARNING DEBUG);
}

1;
