package AllInOne::Util::Log::Screen;

use strict;
use warnings;
use Moo;
use MooX::Types::MooseLike::Base qw(Bool);
use Carp;
use Params::Validate qw(:all);

with 'AllInOne::Util::Log';

has 'debug_mode' => (
    is      => 'ro',
    isa     => Bool,
    default => 0,
    reader  => '_DebugMode'
);

has 'callbacks_everywhere' => (
    is      => 'ro',
    isa     => Bool,
    default => 0,
    reader  => '_CallbacksEveryWhere',
);

sub CRITICAL {
    my $self  = shift;
    my $error = shift;
    croak 'CRITICAL: ' . $error;
}

sub ERROR {
    my $self  = shift;
    my $error = shift;
    $error = 'ERROR: ' . $error;
    $self->_PrintWarning(
        message  => $error,
        callback => 1
    );
    return $self;
}

sub WARNING {
    my $self    = shift;
    my $warning = shift;
    $warning = 'WARNING: ' . $warning;
    $self->_PrintWarning( message => $warning );
    return $self;
}

sub DEBUG {
    my $self  = shift;
    my $debug = shift;
    return $self unless $self->_DebugMode;
    print STDERR 'DEBUG: ' . $debug . "\n";
    return $self;
}

sub _PrintWarning {
    my $self   = shift;
    my %params = validate_with(
        params => \@_,
        spec   => {
            message  => { type => SCALAR },
            callback => {
                type    => SCALAR,
                default => 1,
            }
        }
    );
    my $callback = $params{callback};
    $callback = 1 if $self->_CallbacksEveryWhere;
    my $message = $params{message};
    if ($callback) {
        Carp::cluck $message;
    }
    else {
        warn $message;
    }
}
1;
