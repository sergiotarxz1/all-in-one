#!/usr/bin/env perl
use strict;
use warnings;
use Const::Fast;
use Data::Dumper;
use Test::Most tests => 11;
use Test::MockObject::Extends;

const my %params => (
    driver   => 'Mock',
    host     => 'hola',
    user     => 'mundo',
    port     => 33,
    password => 'mysecret',
    dbname   => 'store',
);
{
    use_ok('AllInOne::DAO::DataBase');
}
{

    my $data_base = AllInOne::DAO::DataBase->new(%params);
    while ( my ( $key, $value ) = each %params ) {
        ok $value eq $data_base->{$key},
          "The parameter $key were registered sucessfully.";
    }
}
{
    my $data_base = AllInOne::DAO::DataBase->new(%params);
    ok ref $data_base->{dh} eq 'DBI::db',
      'The DBI::db object is successfully registered.';
}
{
    my $data_base = AllInOne::DAO::DataBase->new(%params);
    $data_base->{dh} = Test::MockObject::Extends->new( $data_base->{dh} );
    $data_base->{dh}->mock(
        no_array => sub {
            unless (wantarray) {
                return 1;
            }

        }
    );
    my $retorno = $data_base->no_array();
    ok $retorno == 1, 'AUTOLOAD works in scalar context.';
}
{
    my $data_base = AllInOne::DAO::DataBase->new(%params);
    $data_base->{dh} = Test::MockObject::Extends->new( $data_base->{dh} );
    $data_base->{dh}->mock(
        array => sub {
            if (wantarray) {
                return ( 1, 1 );
            } else {
                return;
            }
        }
    );
    my @retorno = $data_base->array();
    my $retorno = $data_base->array();
    ok $retorno[1] == 1, 'AUTOLOAD works in list context';
    ok !defined $retorno, 'AUTOLOAD does the work for scalar context for the second time.';
}
