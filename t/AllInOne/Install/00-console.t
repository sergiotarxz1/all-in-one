#!/usr/bin/env perl
use Test::Most tests=>1;
use strict;
use warnings;

{
    ok `perl lib/AllInOne/Install/Console.pm --help` =~ /\bshows\b/i, 'Install script shows help';
}
