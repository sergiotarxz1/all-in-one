#!/usr/bin/env perl
use Test::Most tests => 3;
use Test::MockModule;

{
    use_ok 'AllInOne::Util::Log::Screen';
}
{
    my $logger = AllInOne::Util::Log::Screen->new;
    throws_ok { $logger->CRITICAL("Hola mundo") } qr/Hola mundo/,
      'Critical throws error';
}
{
    my $logger    = AllInOne::Util::Log::Screen->new;
    my $mock_carp = Test::MockModule->new('Carp');
    $mock_carp->mock(
        cluck => sub {
            my $error = shift;
            ok $error =~ qr/Hola mundo/, 'ERROR works';
        }
    );
    $logger->ERROR('Hola mundo');
};
1;
