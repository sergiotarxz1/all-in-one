package MockLog;

use strict;
use warnings;
use Moo;
use Carp qw(cluck);

with 'AllInOne::Util::Log';

sub CRITICAL {
    my $self  = shift;
    my $error = shift;
    croak $error;
}

sub ERROR {
    my $self  = shift;
    my $error = shift;
    cluck $error if ( $ENV{DEBUG_TEST} );
    push @{ $self->{ERROR} }, $error;
}

sub WARNING {
    my $self  = shift;
    my $error = shift;
    cluck $error if ( $ENV{DEBUG_TEST} );
    push @{ $self->{WARNING} }, $error;
}

sub DEBUG {
    my $self  = shift;
    my $error = shift;
    cluck $error if ( $ENV{DEBUG_TEST} );
    push @{ $self->{DEBUG} }, $error;
}
1;
